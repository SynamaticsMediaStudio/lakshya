import * as React from 'react';
import * as Native from 'react-native';

import * as Paper from 'react-native-paper';

const styles = Native.StyleSheet.create({
    Container: {
    },
    Input:{
        paddingLeft: 30,
        paddingRight: 30,
        marginBottom:5,
    },
    splash:{
        width: "100%",
        height: "100%",
        alignSelf: 'center',
    },
    LoginForm:{
        flex:1,
        justifyContent: 'flex-end',
        alignContent: 'center',
        paddingBottom: 100,
        backgroundColor:"#0e0e0e55"
    },
    formGroup:{
        paddingLeft:30,
        paddingRight:30,
        marginTop:10
    },
    BtnContent:{
        fontFamily:"Poppins-Regular"
    }
  });
export default class Login extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            mobile:"",
            password:"",
            isLoading:false,
        }
        this._login = () => {
            // Validation
            if(!this.state.mobile || !this.state.password){
                let message = "";
                if(!this.state.mobile){
                    message = "Mobile no";
                }
                if(!this.state.password){
                    message = "Password"
                }
                if(!this.state.mobile && !this.state.password){
                    message = "Mobile no and Password";
                }
                Native.ToastAndroid.show(`Your ${message} is required to Login`, Native.ToastAndroid.SHORT);
                return false
            }

            this.setState({ isLoading: true })
        }
    }
    render() {
        return (
            <Native.View>
                <Native.StatusBar translucent={true} backgroundColor="transparent" barStyle="light-content" />
                <Native.ImageBackground  source={{uri :'http://profilepicturesdp.com/wp-content/uploads/2018/06/classical-dance-dp-for-whatsapp-2.jpg'}} style={styles.splash}>
                    <Native.View style={styles.LoginForm}>
                        <Native.View style={styles.formGroup}>
                            <Paper.TextInput  
                                mode="flat" 
                                label='Mobile' 
                                keyboardType="number-pad"
                                value={this.state.mobile} 
                                disabled={this.state.isLoading} 
                                returnKeyType="next"
                                onChangeText={mobile => this.setState({ mobile })} /> 
                        </Native.View>
                        <Native.View style={styles.formGroup}>
                        <Paper.TextInput 
                            secureTextEntry={true}
                            mode="flat" 
                            label='Password' 
                            value={this.state.password} 
                            returnKeyType="go"
                            disabled={this.state.isLoading} 
                            onChangeText={password => this.setState({ password })} />    
                        </Native.View>
                        <Native.View style={styles.formGroup}>
                            <Paper.Button color="black" contentStyle={styles.BtnContent} mode="contained" dark={true} loading={this.state.isLoading} disabled={this.state.isLoading} onPress={this._login}>
                                Login
                            </Paper.Button>
                        </Native.View>
                    </Native.View>
                </Native.ImageBackground>
            </Native.View>
        )
    }
}
