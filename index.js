import * as React from 'react';
import { AppRegistry,StatusBar } from 'react-native';
import { DefaultTheme,Provider as PaperProvider } from 'react-native-paper';
import App from './App';
import {name as appName} from './app.json';
const theme = {
    ...DefaultTheme,
    colors: {
      ...DefaultTheme.colors,
      primary: '#1e3799',
      accent: 'yellow',
    },
  };
export default function Main() {
  return (
      <PaperProvider theme={theme}>
        <App />
      </PaperProvider>
  );
}
AppRegistry.registerComponent(appName, () => Main);
